<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Shipping_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getShippingByID($id)
    {
        $q = $this->db->get_where('shipping_matrixrate', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
	
    public function deleteShipping($id)
    {
        if ($this->db->delete("shipping_matrixrate", array('id' => $id))) {
            return true;
        }
        return FALSE;
    }
	
    public function addShipping($data)
    {
        if ($this->db->insert("shipping_matrixrate", $data)) {
            return true;
        }
        return false;
    }

    public function updateshipping($id, $data = array())
    {
        if ($this->db->update("shipping_matrixrate", $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

}
