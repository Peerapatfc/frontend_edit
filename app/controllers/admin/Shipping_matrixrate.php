<?php defined('BASEPATH') OR exit('No direct script access allowed');

class shipping_matrixrate extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }

        if (!$this->Owner) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect('admin');
        }
        $this->lang->admin_load('settings', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->admin_model('shipping_model');
        $this->upload_path = 'assets/uploads/';
        $this->thumbs_path = 'assets/uploads/thumbs/';
        $this->image_types = 'gif|jpg|jpeg|png|tif';
        $this->digital_file_types = 'zip|psd|ai|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif';
        $this->allowed_file_size = '1024';

    }

    public function index()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('system_settings'), 'page' => lang('system_settings')), array('link' => '#', 'page' => lang('shipping')));
        $meta = array('page_title' => lang('shipping'), 'bc' => $bc);
        $this->page_construct('settings/shipping', $meta, $this->data);
    }
	
    function getShipping()
    {
        $this->load->library('datatables');
        $this->datatables
            ->select("id,ordering, image, condition_name,  condition_from_value, condition_to_value, price, delivery_type")
            ->from("shipping_matrixrate")
            ->add_column("Actions", "<div class=\"text-center\"><a href='" . admin_url('shipping_matrixrate/edit_shipping/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang("edit_shipping") . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("delete_shipping") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('shipping_matrixrate/delete_shipping/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");

        echo $this->datatables->generate();
    }

    function shipping_actions()
    {
        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                // if ($this->input->post('form_action') == 'delete') {
                    // foreach ($_POST['val'] as $id) {
                        // $this->settings_model->deleteBrand($id);
                    // }
                    // $this->session->set_flashdata('message', lang("brands_deleted"));
                    // redirect($_SERVER["HTTP_REFERER"]);
                // }
				
				echo $this->input->post('form_action');
				exit();

            } else {
                $this->session->set_flashdata('error', lang("no_record_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    function add_shipping()
    {
        $this->form_validation->set_rules('condition_from_value', lang("condition_from_value"), 'trim|required');
        if ($this->form_validation->run() == true) {
            $data = array(
                'condition_name' => $this->input->post('condition_name'),
                'condition_from_value' => $this->input->post('condition_from_value'),
                'condition_to_value' => $this->input->post('condition_to_value'),
                'price' => $this->input->post('price'),
                'delivery_type' => $this->input->post('delivery_type'),
                'ordering' => $this->input->post('ordering'),	
                );

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->image_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['max_width'] = $this->Settings->iwidth;
                $config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['image'] = $photo;
                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload_path . $photo;
                $config['new_image'] = $this->thumbs_path . $photo;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = $this->Settings->twidth;
                $config['height'] = $this->Settings->theight;
                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }
                $this->image_lib->clear();
            }
        } elseif ($this->input->post('add_shipping')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("shipping_matrixrate");
        }
        if ($this->form_validation->run() == true && $this->shipping_model->addShipping($data)) {
            $this->session->set_flashdata('message', lang("shipping_add_success"));
            admin_redirect("shipping_matrixrate");
        } else {
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['error'] = validation_errors();
            $this->load->view($this->theme . 'settings/add_shipping', $this->data);
        }
    }
	
	
	
    function edit_shipping($id = NULL)
    {
        if ($this->input->post('id')) {
            $id = $this->input->post('id');
        }
        $this->form_validation->set_rules('condition_from_value', lang("condition_from_value"), 'trim|required');
        if ($this->form_validation->run() == true) {
            $data = array(
                'condition_name' => $this->input->post('condition_name'),
                'condition_from_value' => $this->input->post('condition_from_value'),
                'condition_to_value' => $this->input->post('condition_to_value'),
                'price' => $this->input->post('price'),
                'delivery_type' => $this->input->post('delivery_type'),
                'ordering' => $this->input->post('ordering'),	
                );

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->image_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['max_width'] = $this->Settings->iwidth;
                $config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['image'] = $photo;
                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload_path . $photo;
                $config['new_image'] = $this->thumbs_path . $photo;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = $this->Settings->twidth;
                $config['height'] = $this->Settings->theight;
                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }
                $this->image_lib->clear();
            }
        } elseif ($this->input->post('submit')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("shipping_matrixrate");
        }
        if ($this->form_validation->run() == true && $this->shipping_model->updateshipping($id, $data)) {
            $this->session->set_flashdata('message', lang("shipping_updated"));
            admin_redirect("shipping_matrixrate");
        } else {
			$this->data['shipping'] = $this->shipping_model->getShippingByID($id);
            $this->data['id'] = $id;
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['error'] = validation_errors();
            $this->load->view($this->theme . 'settings/edit_shipping', $this->data);
        }
    }
	
    function updateshipping()
    {
		$id = $this->input->get('id', true);
		$count = $this->input->get('count', true);
        $data = array(
            'ordering' => $count,
        );
		$data = $this->shipping_model->updateshipping($id , $data);

		$this->sma->send_json('000');
	}

    function delete_shipping($id = NULL)
    {
        if ($this->shipping_model->deleteShipping($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("shipping_deleted")));
        }
    }
    

}
